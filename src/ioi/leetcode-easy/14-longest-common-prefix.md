---
title: 14. 最长公共前缀
date: 2020-08-07
problem_no: 14
description: 
timeline: false
article: false
---

<!-- Description. -->

<!-- more -->

## Problem

Source: [LeetCode 14](https://leetcode-cn.com/problems/longest-common-prefix/){target="_blank"}

### Description

编写一个函数来查找字符串数组中的最长公共前缀。

如果不存在公共前缀，返回空字符串 ""。

示例 1：

```text
输入：strs = ["flower","flow","flight"]
输出："fl"
```

示例 2：

```text
输入：strs = ["dog","racecar","car"]
输出：""
解释：输入不存在公共前缀。
```

提示：

- `1 <= strs.length <= 200`
- `0 <= strs[i].length <= 200`
- `strs[i]` 仅由小写英文字母组成

## Solution

分而治之

## Code

@[code cpp](../../_codes/algorithm/code/leet-code/14-main.cpp)
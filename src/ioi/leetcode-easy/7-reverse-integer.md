---
title: 7. 整数反转
problem_no: 7
date: 2020-07-25
description: 
timeline: false
article: false
---

<!-- Description. -->

<!-- more -->

## Problem

Source: [LeetCode 7](https://leetcode-cn.com/problems/reverse-integer/){target="_blank"}

### Description

给你一个 32 位的有符号整数 `x` ，返回将 `x` 中的数字部分反转后的结果。

如果反转后整数超过 32 位的有符号整数的范围 `[−231,  231 − 1]` ，就返回 0。

**假设环境不允许存储 64 位整数（有符号或无符号）。**

示例 1：

```text
输入: 123
输出: 321
```

示例 2：

```text
输入: -123
输出: -321
```

示例 3：

```text
输入: 120
输出: 21
```

示例 4：

```text
输入: 120
输出: 21
```

提示：

- -2^31 <= x <= 2^31 - 1

## Solution

## Code

@[code cpp](../../_codes/algorithm/code/leet-code/7-main.cpp)
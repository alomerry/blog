import { arraySidebar } from "vuepress-theme-hope";

export const md = arraySidebar([
  "",
  "draw",
  "vuepress-extensions",
  "theme-hope-plugins",
]);